# import sys to get env.txt
import sys
# import random to randomly generate snakes and ladders position
import random
# use math.ceil for snakes and ladders positions and math.sqrt for snakes and ladders lengths
import math

"""

VARIABLE MAP

+-----------------------+--------+-----------------------------------------------------------------------------------------------------------+
| NAME                  | TYPE   | PURPOSE                                                                                                   |
+-----------------------+--------+-----------------------------------------------------------------------------------------------------------+
| env                   | dict   | contains the environment variables read from env.txt file                                                 |
+-----------------------+--------+-----------------------------------------------------------------------------------------------------------+
| screen_width          | int    | first argument for size() function; width of screen                                                       |
+-----------------------+--------+-----------------------------------------------------------------------------------------------------------+
| screen_height         | int    | second argument for size() function. Height of screen                                                     |
+-----------------------+--------+-----------------------------------------------------------------------------------------------------------+
| board_width           | int    | number of blocks on the x-axis of the board                                                               |
+-----------------------+--------+-----------------------------------------------------------------------------------------------------------+
| board_height          | int    | number of blocks on the y-axis of the board                                                               |
+-----------------------+--------+-----------------------------------------------------------------------------------------------------------+
| block_size            | int    | width and height for every block (square) on the board                                                    |
+-----------------------+--------+-----------------------------------------------------------------------------------------------------------+
| num_blocks            | int    | total number of blocks on the board; calculated with board_width and board_height                         |
+-----------------------+--------+-----------------------------------------------------------------------------------------------------------+
| geckos_turn           | bool   | True if it's gecko's turn to roll; False if it's rabbit's turn to roll                                    |
+-----------------------+--------+-----------------------------------------------------------------------------------------------------------+
| gecko_pos             | int    | index of gecko's position on the board                                                                    |
+-----------------------+--------+-----------------------------------------------------------------------------------------------------------+
| rabbit_pos            | int    | index of rabbit's position on the board                                                                   |
+-----------------------+--------+-----------------------------------------------------------------------------------------------------------+
| game_finished         | bool   | True if both gecko and rabbit reach the end; False otherwise                                              |
+-----------------------+--------+-----------------------------------------------------------------------------------------------------------+
| font_size             | float  | font_size, adjusted to screen_width, screen_height                                                        |
+-----------------------+--------+-----------------------------------------------------------------------------------------------------------+
| board_boundaries      | list   | list for top-left and bottom-right corners of every block on the board                                    |
+-----------------------+--------+-----------------------------------------------------------------------------------------------------------+
| dice_boundaries       | list   | top-left and bottom-right corners of the dice                                                             |
+-----------------------+--------+-----------------------------------------------------------------------------------------------------------+
| restart_boundaries    | list   | top-left and bottom-right corners of the restart button                                                   |
+-----------------------+--------+-----------------------------------------------------------------------------------------------------------+
| board_numbers_ordered | list   | 1d array of numbers on board ordered in the way it displays (25, 24, 23, 22, 21, 15, 17, 18, 19, 20, ...) |
+-----------------------+--------+-----------------------------------------------------------------------------------------------------------+
| ladder_image          | PImage | PImage object of ladder                                                                                   |
+-----------------------+--------+-----------------------------------------------------------------------------------------------------------+
| dice_image            | PImage | PImage object of dice                                                                                     |
+-----------------------+--------+-----------------------------------------------------------------------------------------------------------+
| rabbit_image          | PImage | PImage object of rabbit                                                                                   |
+-----------------------+--------+-----------------------------------------------------------------------------------------------------------+
| gecko_image           | PImage | PImage object of gecko                                                                                    |
+-----------------------+--------+-----------------------------------------------------------------------------------------------------------+
| restart_image         | PImage | PImage object of restart button                                                                           |
+-----------------------+--------+-----------------------------------------------------------------------------------------------------------+
| roll_num              | int    | number rolled by current player                                                                           |
+-----------------------+--------+-----------------------------------------------------------------------------------------------------------+
| snakes                | list   | on the 2nd dimension: snakes; on the 1st dimension: snake head, snake tail                                |
+-----------------------+--------+-----------------------------------------------------------------------------------------------------------+
| ladders               | list   | on the 2nd dimension: ladders; on the 1st dimension: ladder top, ladder bottom                            |
+-----------------------+--------+-----------------------------------------------------------------------------------------------------------+
| occupied_blocks       | list   | list of snake heads, snake tails, ladder tops, ladder bottoms                                             |
+-----------------------+--------+-----------------------------------------------------------------------------------------------------------+
| gecko_won             | bool   | True if gecko reaches the end; False otherwise                                                            |
+-----------------------+--------+-----------------------------------------------------------------------------------------------------------+
| rabbit_won            | bool   | True if rabbit reaches the end; False otherwise                                                           |
+-----------------------+--------+-----------------------------------------------------------------------------------------------------------+
| player_size           | float  | width and height of each player (gecko and rabbit)                                                        |
+-----------------------+--------+-----------------------------------------------------------------------------------------------------------+

"""

# Get, initialize, and globalize environment variables, create screen
def settings():
    global env
    global screen_width, screen_height, board_width, board_height
    global block_size, num_blocks
    env = getEnvVar()
    screen_width = int(env["screen_width"])
    board_width = int(env["board_width"])
    block_size = screen_width / board_width
    board_height = int(env["board_height"])
    num_blocks = board_width * board_height
    screen_height = screen_width / board_width * board_height
    size(screen_width + block_size, screen_height)

# Set background color, setup everything (font, boundaries, board, images, dice, snakes, ladders, players)
def setup():
    background(200)
    setupFont()
    setupBoundaries()
    setupBoard()
    setupImages()
    setupDice()
    setupSnakes()
    setupLadders()
    setupPlayers()

# Nothing runs in draw(), because all updates to screen are triggered by user interactions
# This function is necessary because the program needs to keep running
def draw():
    return

# Capture mouse release on dice_boundaries and restart_boundaries.
# If the restart button is pressed and the game is finished, reset the game
# If the dice button is pressed and the game is unfinished, roll the current player
# Else ignore mouse release event
def mouseReleased():
    global geckos_turn, gecko_pos, rabbit_pos
    global game_finished
    if mouseX > restart_boundaries[0][0] and mouseX < restart_boundaries[1][0] and mouseY > restart_boundaries[0][1] and mouseY < restart_boundaries[1][1]:
        if game_finished:
            resetGame()
    if mouseX > dice_boundaries[0][0] and mouseX < dice_boundaries[1][0] and mouseY > dice_boundaries[0][1] and mouseY < dice_boundaries[1][1]:
        if game_finished:
            pass
        else:
            roll()
            if gecko_won:
                moveRabbit()
            elif rabbit_won:
                moveGecko()
            elif geckos_turn:
                moveGecko()
            else:
                moveRabbit()
            game_finished = True if gecko_pos == num_blocks and rabbit_pos == num_blocks else False
            if game_finished:
                displayRestart()
            geckos_turn = not geckos_turn
            displayPlayers()

# Get environment variables, put said variables in dict called env, return env
# This is called from settings() at the very beginning
def getEnvVar():
    try:
        file = open("env.txt", "r")
        fileContent = file.readlines()
        env = {}
        for i in range(len(fileContent)):
            pair = fileContent[i].strip("/n").split()
            env[pair[0]] = pair[1]
        return env
    except:
        print("Environment file not found. Exiting")
        sys.exit()

# Adjust font size to screen_width and screen_height
# Setup font using vlw file in data folder
def setupFont():
    global font_size
    font_size = min(screen_width, screen_height) * 0.03
    font = createFont("BrushScriptStd-48.vlw", font_size)
    textFont(font)

# Given board_height and board_width, generate the coordinates of the top left and bottom right corners of each block
# Given board_boundaries, generate coordinates of the top left and bottom right corners of the dice
# Given board_boundaries, generate coordinates of the top left and bottom right corners of the restart button
def setupBoundaries():
    global board_boundaries, dice_boundaries, restart_boundaries
    board_boundaries = []
    for i in range(board_height):
        for j in range(board_width):
            board_boundaries.append([[j * block_size, i * block_size], [(j + 1) * block_size, (i + 1) * block_size]])
    dice_boundaries = [board_boundaries[-1][0][:], board_boundaries[-1][1][:]]
    dice_boundaries[0][0], dice_boundaries[1][0] = dice_boundaries[0][0] + block_size, dice_boundaries[1][0] + block_size
    print(board_boundaries)
    restart_boundaries = [board_boundaries[board_width - 1][0][:], board_boundaries[board_width - 1][1][:]]
    restart_boundaries[0][0] += block_size
    restart_boundaries[1][0] += block_size

# Draw the board according to the top left coordinates of every block in board_boundaries
# Given board_width and board_height, display block numbers
def setupBoard():
    global board_numbers_ordered
    stroke(0)
    strokeWeight(1)
    colors = [[255, 101, 101], [246, 239, 33], [255, 255, 255], [21, 218, 132], [74, 174, 241], [255, 152, 96], [86, 252, 255], [255, 137, 229]]
    for i in range(num_blocks):
        j = i % len(colors)
        fill(colors[j][0], colors[j][1], colors[j][2])
        rect(board_boundaries[i][0][0], board_boundaries[i][0][1], block_size, block_size)
    fill(0)
    board_numbers = [number + 1 for number in range(num_blocks)]
    board_numbers_ordered = []
    for i in range(board_height):
        flip = 1
        if i % 2 == 0:
            flip *= -1
        board_numbers_ordered.extend(board_numbers[board_width * i: board_width * (i + 1)][::flip])
    board_numbers_ordered = board_numbers_ordered[::-1]
    for i in range(num_blocks):
        text(str(board_numbers_ordered[i]), board_boundaries[i][0][0], board_boundaries[i][0][1] + block_size)

# Initialize and globalize all images (PImage objects)
def setupImages():
    global ladder_image, dice_image, rabbit_image, gecko_image, restart_image
    ladder_image = loadImage(env["ladder_image"])
    dice_image = loadImage(env["dice_image"])
    rabbit_image = loadImage(env["rabbit_image"])
    gecko_image = loadImage(env["gecko_image"])
    restart_image = loadImage(env["restart_image"])

# Draw dice image
def setupDice():
    image(dice_image, dice_boundaries[0][0], dice_boundaries[0][1], block_size, block_size)

# Roll a random number between 1 and 6 (inclusive)
# Display rolled number on screen
def roll():
    global roll_num
    roll_num = random.randint(1, 6)
    fill(200)
    noStroke()
    rect(dice_boundaries[0][0] + 1, dice_boundaries[0][1] - block_size, block_size, block_size)
    fill(0)
    text(str(roll_num), dice_boundaries[0][0] + (block_size / 2) - font_size / 4, dice_boundaries[0][1] - (block_size / 2) + font_size / 4)

# Generate snake locations based on block indices and blocks-to-snakes ratio, globalize variable called snakes
# Call drawSnakes() function after
def setupSnakes():
    global snakes, occupied_blocks
    snakes = []
    snakes_ratio = int(env["snakes_ratio"])
    occupied_blocks = []
    for i in range(num_blocks // snakes_ratio):
        head = float(random.randint(board_width + 1, num_blocks))
        while head in occupied_blocks or head == num_blocks:
            head = float(random.randint(board_width + 1, num_blocks))
        occupied_blocks.append(head)
        head_row = math.ceil(head / board_width)
        tail = random.randint(1, (head_row - 1) * board_width)
        while tail in occupied_blocks or tail == num_blocks:
            tail = random.randint(1, (head_row - 1) * board_width)
        occupied_blocks.append(tail)
        snakes.append([int(tail), int(head)])
    drawSnakes()

# Draw a dark translucent line for the snakes
def drawSnakes():
    strokeWeight(min(screen_width, screen_height) * 0.025)
    stroke(10, 10, 10, 180)
    for snake in snakes:
        snake_length = math.sqrt((board_boundaries[snake[0] - 1][0][0] - board_boundaries[snake[1] - 1][0][0]) ** 2 + (board_boundaries[snake[0] - 1][0][1] - board_boundaries[snake[1] - 1][0][1]) ** 2)
        head_x, head_y = board_boundaries[board_numbers_ordered.index(snake[0])][0]
        tail_x, tail_y = board_boundaries[board_numbers_ordered.index(snake[1])][0]
        line(head_x + block_size / 2, head_y + block_size / 2, tail_x + block_size / 2, tail_y + block_size / 2)

# Generate ladder locations based on block indices and blocks-to-ladders ratio, globalize variable called ladders
# Call drawLadders() function after
def setupLadders():
    global ladders, occupied_blocks
    ladders = []
    ladders_ratio = int(env["ladders_ratio"])
    for i in range(num_blocks // ladders_ratio):
        top = float(random.randint(board_width + 1, num_blocks))
        while top in occupied_blocks or top == num_blocks:
            top = float(random.randint(board_width + 1, num_blocks))
        occupied_blocks.append(top)
        top_row = math.ceil(top / board_width)
        bottom = random.randint(1, (top_row - 1) * board_width)
        while bottom in occupied_blocks or bottom == num_blocks:
            bottom = random.randint(1, (top_row - 1) * board_width)
        occupied_blocks.append(bottom)
        ladders.append([int(bottom), int(top)])
    drawLadders()

# Draw a light translucent line for the ladders
def drawLadders():
    strokeWeight(min(screen_width, screen_height) * 0.025)
    stroke(245, 245, 245, 180)
    for ladder in ladders:
        ladder_length = math.sqrt((board_boundaries[ladder[0] - 1][0][0] - board_boundaries[ladder[1] - 1][0][0]) ** 2 + (board_boundaries[ladder[0] - 1][0][1] - board_boundaries[ladder[1] - 1][0][1]) ** 2)
        top_x, top_y = board_boundaries[board_numbers_ordered.index(ladder[0])][0]
        bottom_x, bottom_y = board_boundaries[board_numbers_ordered.index(ladder[1])][0]
        line(top_x + block_size / 2, top_y + block_size / 2, bottom_x + block_size / 2, bottom_y + block_size / 2)

# Initialize and globalize variables necessary for players in gameplay
def setupPlayers():
    global game_finished
    global gecko_won, rabbit_won
    global geckos_turn, player_size
    global gecko_pos, rabbit_pos
    game_finished = False
    gecko_won = False
    rabbit_won = False
    geckos_turn = True
    player_size = block_size / 1.5
    gecko_pos = 0
    rabbit_pos = 0

# Draw gecko and rabbit on screen based on their positions
# Their positions are correlated with indices in board_numbers_ordered
def displayPlayers():
    coverPreviousPlayers()
    print("gecko_pos", gecko_pos, "rabbit_pos", rabbit_pos)
    gecko_index = board_numbers_ordered.index(gecko_pos)
    gecko_x, gecko_y = board_boundaries[gecko_index][0]
    image(gecko_image, gecko_x, gecko_y, player_size, player_size)
    if rabbit_pos != 0:
        rabbit_index = board_numbers_ordered.index(rabbit_pos)
        rabbit_x, rabbit_y = board_boundaries[rabbit_index][0]
        rabbit_x += block_size / 3.4
        rabbit_y += block_size / 3.4
        image(rabbit_image, rabbit_x, rabbit_y, player_size, player_size)

# Cover previously drawn players
def coverPreviousPlayers():
    setupBoard()
    drawSnakes()
    drawLadders()

# Given the number rolled, previous gecko position and positions of snakes and ladders, determine and update the next location of gecko
def moveGecko():
    global gecko_pos, roll_num, gecko_won
    if (gecko_pos + roll_num) == num_blocks:
        gecko_pos += roll_num
        gecko_won = True
        print("gecko finished")
    elif (gecko_pos + roll_num) < num_blocks:
        gecko_pos += roll_num
        if gecko_pos in occupied_blocks:
            snake_tail = None
            ladder_top = None
            for snake in snakes:
                if gecko_pos == snake[1]:
                    snake_tail = snake[0]
                    break
            if snake_tail == None:
                for ladder in ladders:
                    if gecko_pos == ladder[0]:
                        ladder_top = ladder[1]
            if snake_tail != None:
                gecko_pos = snake_tail
                print("gecko snaked")
            elif ladder_top != None:
                gecko_pos = ladder_top
                print("gecko laddered")

# Given the number rolled, previous rabbit position and positions of snakes and ladders, determine and update the next location of rabbit
def moveRabbit():
    global rabbit_pos, roll_num, rabbit_won
    if (rabbit_pos + roll_num) == num_blocks:
        rabbit_pos += roll_num
        rabbit_won = True
        print("rabbit finished")
    elif (rabbit_pos + roll_num) < num_blocks:
        rabbit_pos += roll_num
        if rabbit_pos in occupied_blocks:
            snake_tail = None
            ladder_top = None
            for snake in snakes:
                if rabbit_pos == snake[1]:
                    snake_tail = snake[0]
                    break
            if snake_tail == None:
                for ladder in ladders:
                    if rabbit_pos == ladder[0]:
                        ladder_top = ladder[1]
            if snake_tail != None:
                rabbit_pos = snake_tail
                print("rabbit snaked")
            elif ladder_top != None:
                rabbit_pos = ladder_top
                print("rabbit laddered")

# Draw restart button
def displayRestart():
    image(restart_image, restart_boundaries[0][0], 0, block_size, block_size)

# Cover restart button
def coverRestart():
    fill(200)
    noStroke()
    rect(restart_boundaries[0][0] + 1, restart_boundaries[0][1], block_size, block_size)

# After the restart button is pressed, restart the game (re-randomize snakes and ladders, reset players positions and redraw everything)
def resetGame():
    setupBoard()
    setupSnakes()
    setupLadders()
    setupPlayers()
    coverRestart()
    # cover previous rolled number
    rect(dice_boundaries[0][0] + 1, dice_boundaries[0][1] - block_size, block_size, block_size)
